package main

import (
	"fmt"
	"os"
	"image"
)

func cli() {
	if hasPipeInpput() {
		runFromPipe()
	}

	if len(os.Args) > 1 {
		runFromCommand()
	}
}

func hasPipeInpput() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode() & os.ModeCharDevice == 0
}

func printHelp() {
	fmt.Println("COMMANDS :")
	fmt.Println("  file /path/to/image         Scan QR codes from an image file")
	fmt.Println("  help                        Show help")
	fmt.Println("  screen                      Scan QR codes on screen")
	fmt.Println("  version                     Show version")
}

func printResult(result []*string) {
	if len(result) == 0 {
		fmt.Println("Code not found")
		os.Exit(1);
	}
	for i := 0; i < len(result); i++ {
		fmt.Println(*result[i])
	}
	os.Exit(0)
}

func runFromCommand() {
	switch os.Args[1] {
	case "screen":
		qrcodes := scanScreen()
		printResult(qrcodes)
	case "file":
		if len(os.Args) < 3 {
			fmt.Println("Image path missing\n")
			printHelp()
			os.Exit(2)
		}
		image, err := getImageFromPath(os.Args[2])
		if err != nil {
			fmt.Println(err)
			os.Exit(2)
		}
		qrcodes := parseImage(image)
		printResult(qrcodes)
	case "help":
		printHelp()
	case "version":
		fmt.Println("Version " + Version)
	default:
		fmt.Println("Bad command\n")
		printHelp()
		os.Exit(2)
	}
	os.Exit(0)
}

func runFromPipe() {
	img, _, err := image.Decode(os.Stdin)
	if err != nil {
		fmt.Println(err);
		os.Exit(2)
	}
	qrcodes := parseImage(img)
	printResult(qrcodes)
}
