package main

import (
	"github.com/kbinani/screenshot"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/multi/qrcode"
	"image"
	"os"
	"strings"
)

const Version = "1.3.0"

func main() {
	cli()
	gui()
}

func getImageFromPath(path string) (image.Image, error) {
	f, err := os.Open(path)
	if err != nil {
			return nil, err
	}
	defer f.Close()
	image, _, err := image.Decode(f)
	return image, err
}

func parseImage(img image.Image) []*string {
	bmp, _ := gozxing.NewBinaryBitmapFromImage(img)
	reader := qrcode.NewQRCodeMultiReader()
	decoded, _ := reader.DecodeMultiple(bmp, nil)
	results := make([]*string, 0)
	for i := 0; i < len(decoded); i++ {
		if decoded[i] != nil {
			text := strings.Replace(decoded[i].GetText(), "\r", "", -1)
			results = append(results, &text)
		}
	}
	return results
}

func scanScreen() []*string {
	qrcodes := make([]*string, 0)

	for i := 0; i < screenshot.NumActiveDisplays(); i++ {
		bounds := screenshot.GetDisplayBounds(i)
		img, err := screenshot.CaptureRect(bounds)
		if err != nil {
			panic(err)
		}
		qrcodes = append(qrcodes, parseImage(img)...)
	}

	return qrcodes
}
