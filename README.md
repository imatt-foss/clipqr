# ClipQR

Simple app to **scan QR** codes **on screen** and **from camera**, the result is in your clipboard.

Works as GUI and CLI, with multiple QR codes and multiple monitors.

Free & open source, no tracking, no telemetry.

## Usage

### GUI

If no argument is provided, the application starts with a GUI.  

![App screenshot](screenshot.png)

_Tip: it support pasting an image from your clipboard or dragging a file onto the window_

### CLI

ClipQR also works as a command line without starting the GUI, the result is simply output:

```sh
# Scan an image file
clipqr file /path/to/file.png

# Scan an image from a pipe
cat /path/to/file.png | clipqr

# Scan screen
clipqr screen
```

The following exit status codes are used:

* Status 0: code(s) found
* Status 1: code not found
* Status 2: an error occurred

## Installation

Any help for packaging ClipQR would be appreciated, here's what's available:

### GNU/Linux

Available on [Nixpkgs](https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query=clipqr) (recommended) and [Flathub](https://flathub.org/apps/details/ch.imatt.ClipQR) (currently outdated)

## Build

To run or build ClipQR from source code, [install Go](https://go.dev/doc/install) or load development environment [using Nix](https://nixos.org/download.html#download-nix) :

```sh
nix develop .
```

You can then :

```sh
# Run the app
go run .

# Build for your current system
go build -o bin/clipqr .

# Build for specific platform/architecture
GOOS=linux GOARCH=amd64 go build -o bin/clipqr-linux-amd64 .
```

## Why another QR reader ?

We needed to scan QR codes on screen (for Swiss QR invoices), and didn't find anything available.

All the hard work is done by [go modules](go.mod), thanks to them !

## Contribute

Contributions are welcome, feel free to open an issue for bugs or feature requests.

## Privacy Policy

We do not collect any data neither from you or about you.

Camera and screen recording are used exclusively to find QR codes, images are processed in memory and nothing is persisted on your disk.
