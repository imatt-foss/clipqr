package main

import (
	"bytes"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/pion/mediadevices"
	"github.com/pion/mediadevices/pkg/prop"
	"golang.design/x/clipboard"
	"golang.org/x/image/draw"
	"image"
	"image/png"

	// This is required to register camera adapter
	_ "github.com/pion/mediadevices/pkg/driver/camera"
)

var canvasImg *canvas.Image
var content *fyne.Container
var copyButtons = make([]*widget.Button, 0)
var scanCamButton *widget.Button
var scanScreenButton *widget.Button
var status *widget.Label
var toolbar *fyne.Container
var window fyne.Window

func gui() {
	app := app.New()
	window = app.NewWindow("ClipQR")

	status = widget.NewLabel("Choose an action")
	toolbar = container.NewHBox()

	content = container.NewVBox(
		toolbar,
		status,
	)

	scanScreenButton = widget.NewButton("Scan screen", func() {
		removeCopyButtons()
		setStatus("Running...")
		results := scanScreen()
		setResults(results)
	})

	scanCamButton = widget.NewButton("Scan from camera", func() {
		disableToolbar()
		status.Hide()
		go scanCam()
	})

	toolbar.Add(scanScreenButton)
	toolbar.Add(scanCamButton)

	window.SetContent(content)
	window.SetOnDropped(fileDragged)

	handlePaste()
	window.ShowAndRun()
}

func disableToolbar() {
	scanCamButton.Disable()
	scanScreenButton.Disable()
}

func enableToolbar() {
	scanCamButton.Enable()
	scanScreenButton.Enable()
}

func fileDragged(position fyne.Position, uris []fyne.URI) {
	removeCopyButtons()
	setStatus("Running...")

	qrcodes := make([]*string, 0)
	for i := 0; i < len(uris); i++ {
		image, err := getImageFromPath(uris[i].Path())
		if err != nil {
			continue
		}
		qrcodes = append(qrcodes, parseImage(image)...)
	}

	setResults(qrcodes)
}

func handlePaste() {
	err := clipboard.Init()
	if err == nil {
		window.Canvas().AddShortcut(&fyne.ShortcutPaste{}, func(shortcut fyne.Shortcut) {
			imageByte := clipboard.Read(clipboard.FmtImage)
			image, err := png.Decode(bytes.NewReader(imageByte))
		  if err != nil {
				return
		  }
			qrcodes := parseImage(image)
			setResults(qrcodes)
		})
	}
}

func removeCopyButtons() {
	for i := 0; i < len(copyButtons); i++ {
		toolbar.Remove(copyButtons[i])
	}
	copyButtons = make([]*widget.Button, 0)
}

func resize() {
	window.Resize(content.MinSize())
	window.Show() // helps to resize window in some Linux Window Managers
}

func setResults(results []*string) {
	if len(results) == 0 {
		setStatus("Code not found")
	} else {
		err := clipboard.Init()
		if err != nil {
			panic(err)
		}

		concatenateResults := ""
		for i := 0; i < len(results); i++ {
			concatenateResults += *results[i]
			if i + 1 < len(results) {
				concatenateResults += "\n"
			}

			r := results[i]
			copyButton := widget.NewButtonWithIcon("", theme.ContentCopyIcon(), func() {
				clipboard.Write(clipboard.FmtText, []byte(*r))
			})

			toolbar.Add(copyButton)
			copyButtons = append(copyButtons, copyButton)
		}

		clipboard.Write(clipboard.FmtText, []byte(concatenateResults))

		if len(results) == 1 {
			setStatus("Copied to clipboard !")
		} else {
			setStatus("Multiple codes copied to clipboard !")
		}
	}
	go resize()
}

func setStatus(text string) {
	status.SetText(text)
	status.Show()
}

func scaleImage(src image.Image, rect image.Rectangle, scale draw.Scaler) image.Image {
	newImage := image.NewRGBA(rect)
	scale.Scale(newImage, rect, src, src.Bounds(), draw.Over, nil)
	return newImage
}

func scanCam() {
	removeCopyButtons()

	stream, _ := mediadevices.GetUserMedia(mediadevices.MediaStreamConstraints{
		Video: func(constraint *mediadevices.MediaTrackConstraints) {
			constraint.Width = prop.Int(5000)
			constraint.Height = prop.Int(5000)
		},
	})

	if stream == nil {
		setStatus("No camera available")
		enableToolbar();
		return
	}

	canceled := false
	track := stream.GetVideoTracks()[0]
	videoTrack := track.(*mediadevices.VideoTrack)
	defer videoTrack.Close()

	videoReader := videoTrack.NewReader(false)
	frame, release, _ := videoReader.Read()
	defer release()

	bounds := frame.Bounds()
	width := 500
	height := bounds.Max.Y * width / bounds.Max.X

	cancelButton := widget.NewButton("Cancel", func() {
		canceled = true
	})
	toolbar.Add(cancelButton)

	if (canvasImg == nil) {
		scaled := scaleImage(frame, image.Rect(0, 0, width, height), draw.ApproxBiLinear)
		canvasImg = canvas.NewImageFromImage(scaled)
		canvasImg.FillMode = canvas.ImageFillOriginal
		content.Add(canvasImg)
	} else {
		canvasImg.Show()
	}

	for {
		frame, release, _ := videoReader.Read()
		defer release()

		qrcodes := parseImage(frame)
		if len(qrcodes) > 0 || canceled {
			canvasImg.Hide()
			toolbar.Remove(cancelButton)
			enableToolbar()
			setResults(qrcodes)
			return
		}

		scaled := scaleImage(frame, image.Rect(0, 0, width, height), draw.ApproxBiLinear)
		canvasImg.Image = scaled
		canvasImg.Refresh()
	}
}
