{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { flake-utils, nixpkgs, self }:
  flake-utils.lib.eachDefaultSystem (system:
  let
    pkgs = nixpkgs.legacyPackages.${system};
    libs = with pkgs; [
      libGL
      mesa
      xorg.libX11
      xorg.libXcursor
      xorg.libXext
      xorg.libXi
      xorg.libXinerama
      xorg.libXrandr
      xorg.libXxf86vm
    ];
  in
  rec {
    packages = flake-utils.lib.flattenTree {
      clipqr = let lib = pkgs.lib; in
      pkgs.buildGoModule {
        pname = "clipqr";
        version = "1.3.0";
        vendorHash = null;
        src = ./.;

        nativeBuildInputs = with pkgs; [ copyDesktopItems pkg-config ];
        buildInputs = [ pkgs.libGL ] ++ libs;
        desktopItems = [
          (pkgs.makeDesktopItem {
          name = "ClipQR";
          desktopName = "ClipQR";
          exec = "clipqr";
          categories = [ "Utility" ];
          icon = "clipqr";
          comment = "Scan QR codes on screen and from camera";
          genericName = "ClipQR";
          })
        ];

        postInstall = ''
          install -Dm644 icon.svg $out/share/icons/hicolor/scalable/apps/clipqr.svg
        '';

        meta = {
          description = "Simple app to scan QR codes on screen and from camera, the result is in your clipboard";
          homepage = "https://gitlab.com/imatt-foss/clipqr";
          license = lib.licenses.mit;
          maintainers = [ "MatthieuBarthel" ];
          platforms = lib.platforms.linux ++ lib.platforms.darwin;
        };
      };
    };

    defaultPackage = packages.clipqr;
    defaultApp = packages.clipqr;

    devShell = pkgs.mkShell {
      packages = with pkgs; [
        pkg-config
        golint
        go
      ] ++ libs;
    };
  }) // {
    overlay = final: prev: {
      clipqr = self.packages.${final.system}.clipqr;
    };
  };
}
