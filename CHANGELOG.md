# Changelog

## [1.3.0] - 2024-08-22

* Fix wayland issues
* Update dependencies

## [1.2.0] - 2023-12-21

* Add support for CLI usage
* Allow to paste an image from the clipboard

## [1.1.1] - 2023-11-20

* Fix for flatpak packaging

## [1.1.0] - 2023-11-19

* Add multiple QR scan support
* Drag an image file to scan it
* Update packages

## [1.0.1] - 2022-07-23

### Fix

* Minor changes for packaging

## [1.0.0] - 2022-07-18

_Initial version_
